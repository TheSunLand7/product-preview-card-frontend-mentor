/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			colors: {
				"dark-cyan": "hsl(158, 36%, 37%)",
				"cream": "hsl(30, 38%, 92%)",
				"very-dark-blue": "hsl(212, 21%, 14%)",
				"dark-graysih-blue": "hsl(228, 12%, 48%)",
				"white": " hsl(0, 0%, 100%)",
			},
			fontFamily: {
				"montserrat": ['Montserrat', "sans-serif"],
				"fraunces": ['Fraunces', "serif"],
			},
			screens: {
				"desktop": '1440px',
			}
		},
	},
	plugins: [],
}
