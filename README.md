# Frontend Mentor - Product preview card component solution

This is a solution to the [Product preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-preview-card-component-GO7UmttRfa). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Frontend Mentor - Product preview card component solution](#frontend-mentor---product-preview-card-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
  - [Author](#author)
  - [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

-   View the optimal layout depending on their device's screen size
-   See hover and focus states for interactive elements

### Screenshot

-   Desktop design
    ![Desktop design](/public/images/desktop-design.png)

-   Mobile design
    ![Mobile Design](public/images/mobile-design.png)

### Links

-   Solution URL: [Gitlab](https://gitlab.com/TheSunLand7/product-preview-card-frontend-mentor)
-   Live Site URL: [Vercel](https://product-preview-card-frontend-mentor.vercel.app/)

## My process

### Built with

-   Semantic HTML5 markup
-   CSS custom properties
-   Flexbox
-   CSS Grid
-   Mobile-first workflow
-   [Astro](https://astro.build/) - Static Site Generator
-   [Tailwind CSS](https://tailwindcss.com/) - CSS framework
-   [PostCSS](https://postcss.org/) - CSS pre-processor

### What I learned

I am preparing my path to become a web developer, so this project helped me to practice Tailwindcss, postcss and Astro. I am no very good at Astro but I think I can improve my skills with this kind of projects. I had problems with postcss-nesting, after reading documentation from Tailwindcss and postcss, finally I could resolve it.

### Continued development

I'd like to master Tailwind CSS and Astro, because they are kind of easy to learn. After that I am thinking to learn Next.js, React, Vue and module bundlers like Vite and webpack. This is a long way.

## Author

-   Website - [xiayudev](https://xiayudevsportfoliov2.netlify.app/)
-   Twitter - [@J7Jeo](https://twitter.com/J7Jeo)
-   Github - [xiayudev](https://github.com/xiayudev)

## Acknowledgments

One tip that helped me to resolve this is using flex for mobile design and grid for desktop design. I hope this little tip can help you.
